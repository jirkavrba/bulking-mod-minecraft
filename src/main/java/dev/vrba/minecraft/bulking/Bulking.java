package dev.vrba.minecraft.bulking;

import com.mojang.serialization.Lifecycle;
import net.fabricmc.api.ModInitializer;
import net.minecraft.item.BlockItem;
import net.minecraft.item.FoodComponent;
import net.minecraft.item.Item;
import net.minecraft.registry.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.List;

public class Bulking implements ModInitializer {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void onInitialize() {
        logger.info("Initializing bulking mod");

        this.makeAllBlocksRegularItems();
        this.makeAllItemsEdible();
    }

    private void makeAllBlocksRegularItems() {
        logger.info("Transforming all block items into regular items");

        Registries.ITEM.stream()
                .filter(BlockItem.class::isInstance)
                .map(BlockItem.class::cast)
                .forEach(this::transformBlockIntoRegularItem);
    }

    private void transformBlockIntoRegularItem(@NotNull BlockItem item) {
        final var items = Registries.ITEM;

        @SuppressWarnings("unchecked")
        final var registry = (MutableRegistry<Item>) items;
        final var key = registry.getKey(item).orElseThrow();

        final var settings = new Item.Settings();
        final var base = new Item(settings);

        registry.set(items.getRawId(item), key, base, Lifecycle.stable());

        logger.info("Replaced block item {} in the items registry", item.getBlock().getName());

        System.out.println(registry.getId(base));
        System.out.println(registry.get(registry.getId(base)));
    }

    private void makeAllItemsEdible() {
        Registries.ITEM.stream().forEach(this::makeItemEdible);
    }

    private void makeItemEdible(@NotNull Item item) {
        try {
            final var reflection = item.getClass();
            final var field = reflection.getDeclaredField("foodComponent");
            final var food = FoodComponent.class
                    .getDeclaredConstructor(int.class, float.class, boolean.class, boolean.class, boolean.class, List.class)
                    .newInstance(1, 1, false, true, true, Collections.emptyList());

            field.setAccessible(true);
            field.set(item, food);
        }
        catch (NoSuchFieldException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException exception) {
            // Nah, this won't happen :clueless:
            System.out.println(exception.getMessage());
        }
    }
}
